/*
Sequence 3
Avery Ali
Date submitted:10/7/2014
Purpose: Define and implement the sequence class as provided 
using linked lists.
*/

#include <cstdlib>
#include <cassert>
#include "Ali_sequence3.v0.h"
#include "node1.h"

using namespace main_savitch_5;
using namespace std;

sequence::sequence()
    {
        head_ptr = NULL;
        tail_ptr = NULL;
        cursor = NULL;
        precursor = NULL;
        many_nodes = 0;
    }


    sequence::sequence(const sequence& source)
    {
        node *tail_ptr;

        list_copy(source.head_ptr, head_ptr, tail_ptr);

        many_nodes = source.many_nodes;

        cursor = source.cursor;

        precursor = source.precursor;
    }

    sequence::~sequence()
    {
        list_clear(head_ptr);
        many_nodes = 0;
    }

    //MODIFICATION MEMBER FUNCTIONS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
    void sequence::start()
    {
        if(head_ptr == NULL)
        {
            return;
        }

        else
        {
            precursor = NULL;
            cursor = head_ptr;
        }
    }

    void sequence::advance()
    {
        assert(is_item());

        if(cursor == tail_ptr)
        {
            cursor = NULL;
            precursor = NULL;
        }

        else
        {
            precursor = cursor;
            cursor = cursor -> link();
        }
    }

    void sequence::insert(const value_type& Nentry)
    {
        if (is_item( ))
        {
            if(precursor == NULL || cursor == NULL)
            {
                list_head_insert(head_ptr, Nentry);

                cursor = head_ptr;
                precursor = NULL;
            }

            else
            {
                list_insert(precursor, Nentry);

                cursor = head_ptr;
            }
        }
        else
        {
            list_head_insert(head_ptr, Nentry);

            cursor = head_ptr;

            precursor = NULL;
        }

            many_nodes++;
    }

        void sequence::attach(const value_type& newEntry)
        {
            if(is_item())
            {
                precursor = cursor;

                list_insert(cursor, newEntry);

                cursor = cursor -> link();
            }

            else
            {
                if(head_ptr == NULL)
                {
                    list_head_insert(head_ptr, newEntry);

                    cursor = head_ptr;

                    precursor = NULL;
                }
                else
                {
                    precursor = list_locate(head_ptr, list_length(head_ptr));

                    list_insert(precursor,  newEntry);

                    cursor = precursor -> link();
                }
            }

            many_nodes++;
        }

        void sequence::remove_current()
        {
            if(cursor == head_ptr)
            {
                cursor = cursor -> link();
                list_head_remove(head_ptr);
            }
            else
            {
                cursor = cursor -> link();
                list_remove(head_ptr);
            }

            many_nodes--;
        }

        void sequence::operator =(const sequence& source)
        {
            if(this == &source)
            {
                return;
            }
            else
            {
                list_copy(source.head_ptr, head_ptr, tail_ptr);

                if(source.precursor == NULL)
                {
                    if(source.cursor == NULL)
                    {
                        cursor = NULL;
                        precursor = NULL;
                    }
                    else
                    {
                        cursor = head_ptr;
                        precursor = NULL;
                    }
                }
                else
                {
                    node *tempPtr = head_ptr;
                    node *sourcePtr = source.head_ptr;

                    while(sourcePtr != source.precursor)
                    {
                        sourcePtr = sourcePtr -> link();

                        tempPtr = tempPtr -> link();
                    }

                    cursor = tempPtr -> link();
                    precursor = tempPtr;
                }
            }

            many_nodes = source.many_nodes;
        }
        //CONSTANT MEMBER FUNCTIONS                
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    

        sequence::value_type sequence::current() const
        {
            return cursor -> data();
        }

