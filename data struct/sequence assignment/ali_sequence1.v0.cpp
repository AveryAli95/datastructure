//Avery Ali sequence
#include <cstdlib>
#include <cassert>
#include "main_savitch_sequence.h"

using namespace std;
namespace main_savitch_3
{
	
//value_type data[CAPACITY];
//size_type used; # of items in sequence
//size_type current_index;

//   bool is_item( ) const
//     Postcondition: A true return value indicates that there is a valid
//     "current" item that may be retrieved by activating the current
//     member function (listed below). A false return value indicates that
//     there is no valid current item.
	bool sequence :: is_item() const
	{
		return (current_index < used); //make sure current is less than the number of items in sequence
	}
	
//   size_type size( ) const
//     Postcondition: The return value is the number of items in the sequence.
	sequence::size_type sequence::size() const
	{
		return used;
	}
	
//   value_type current( ) const
//     Precondition: is_item( ) returns true.
//     Postcondition: The item returned is the current item in the sequence.
	sequence::value_type sequence::current() const
	{
		assert( is_item() == true);
		return data[current_index];

	}
	
	//default constuctor (set private data = to 0)	
	sequence :: sequence() 
	{
		used = 0;
		current_index=0;
	}

// void start( )
// Postcondition: The first item on the sequence becomes the current item
//(but if the sequence is empty, then there is no current item).
	void sequence :: start()
	{
		current_index = 0;
	}

//   void advance( )
//     Precondition: is_item returns true.
//     Postcondition: If the current item was already the last item in the
//     sequence, then there is no longer any current item. Otherwise, the new
//     current item is the item immediately after the original current item.

	void sequence :: advance()
	{
		assert(is_item() == true);
		current_index++;
	}

//   void insert(const value_type& entry)
//     Precondition: size( ) < CAPACITY.
//     Postcondition: A new copy of entry has been inserted in the sequence
//     before the current item. If there was no current item, then the new entry 
//     has been inserted at the front of the sequence. In either case, the newly
//     inserted item is now the current item of the sequence.
	void sequence :: insert(const value_type& entry)
	{
		
		assert( size() < CAPACITY);
		if( is_item() == false)
		{ 
			current_index = 0; 
		}

		for(size_type j = used; j > current_index; j--)
		{
			data[j] = data[j-1]; //moves everything from current position down one and creates and "empty" space to insert new number
		}

		data[current_index] = entry; //inserts new number in current position
		used++; //sequence just got larger by one so increase used by 1

	}

//   void attach(const value_type& entry)
//     Precondition: size( ) < CAPACITY.
//     Postcondition: A new copy of entry has been inserted in the sequence after
//     the current item. If there was no current item, then the new entry has 
//     been attached to the end of the sequence. In either case, the newly
//     inserted item is now the current item of the sequence.
   
	void sequence::attach(const value_type& entry)
   {
     
      assert( size( ) < CAPACITY );

      if ( is_item() == false )
         {
			 current_index = used-1; //puts current to last number in sequence
	     }
      
	  for (size_type k = used; k > current_index; k-- )
         {
			 data[k] = data[k-1];
	     }
      
	  current_index++;
      data[current_index] = entry;
      used++;
   }

//   void remove_current( )
//     Precondition: is_item returns true.
//     Postcondition: The current item has been removed from the sequence, and the
//     item after this (if there is one) is now the new current item.
   void sequence::remove_current( )
   {
      
      assert( is_item( ) == true );

      for (size_type i = current_index + 1; i < used; i++ )
         {
			 data[i-1] = data[i];
	     }
     
	  used--;
   }
}//end of file