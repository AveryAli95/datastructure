// FILE: table2.h
// TEMPLATE CLASS PROVIDED: Table<RecordType>
//   This class is a container template class for a Table of records.
//   The template parameter, RecordType, is the data type of the records in the
//   Table. It may any type with a default constructor, a copy constructor,
//   an assignment operator, and an integer member variable called key.
//
// CONSTRUCTOR for the Table<RecordType> template class:
//   Table( )
//     Postcondition: The Table has been initialized as an empty Table.
//
// MODIFICATION MEMBER FUNCTIONS for the Table<RecordType> class:
//   void insert(const RecordType& entry)
//     Precondition: entry.key >= 0. Also if entry.key is not already a key in
//     the table, then the Table has space for another record
//     (i.e., size( ) < CAPACITY).
//     Postcondition: If the table already had a record with a key equal to
//     entry.key, then that record is replaced by entry. Otherwise, entry has
//     been added as a new record of the Table.
//
//   void remove(int key)
//     Postcondition: If a record was in the Table with the specified key, then
//     that record has been removed; otherwise the table is unchanged.
//
// CONSTANT MEMBER FUNCTIONS for the Table<RecordType> class:
//   bool is_present(const Item& target) const
//     Postcondition: The return value is true if there is a record in the
//     Table with the specified key. Otherwise, the return value is false.
//
//   void find(int key, bool& found, RecordType& result) const
//     Postcondition: If a record is in the Table with the specified key, then
//     found is true and result is set to a copy of the record with that key.
//     Otherwise found is false and the result contains garbage.
//
//   size_t size( ) const
//     Postcondition: Return value is the total number of records in the
//     Table.
//
// VALUE SEMANTICS for the Table<RecordType> template class:
//   Assignments and the copy constructor may be used with Table objects.
//
// DYNAMIC MEMORY USAGE by the Table<RecordType> template class:
//   If there is insufficient dynamic memory, then the following functions
//   can call new_handler: the copy constructor, insert, the assignment
//   operator.

#ifndef TABLE2_H
#define TABLE2_H

#include <cassert> // Provides assert
#include <cstdlib>    // Provides size_t
#include <vector>
#include "lzwhelp.h"

namespace proj5
{
    template <class RecordType>
    class table
    {
    public:
        // MEMBER CONSTANT
        static const size_t TABLE_SIZE = MAXSIZE;
        // CONSTRUCTORS and DESTRUCTOR
        table( );
        // MODIFICATION MEMBER FUNCTIONS
        void insert(const RecordType& entry);
        void remove(int key);
        // CONSTANT MEMBER FUNCTIONS
        bool find(int key, RecordType& result) const;
        RecordType* find(int key);
        RecordType operator [](size_t i) const;
        RecordType& operator [](size_t i);
        bool is_present(int key) const;
        size_t size( ) const { return total_records; }
    private:
        std::vector<RecordType> data[TABLE_SIZE];
        size_t total_records;
        // HELPER FUNCTION
        size_t hash(int key) const;
    };

    //Implementation
	template <class RecordType>
	table<RecordType>::table( )
	// Library facilities used: stdlib.h
	{

		total_records = 0;

		//Do not need to initialize data array since vectors initialize to EMPTY
	}

	template <class RecordType>
	void table<RecordType>::insert(const RecordType& entry)
	// Library facilities used: assert.h, link2.h
	{
		assert(entry.key >= 0);
		total_records++;
		//data[hash(entry.key)].push_back(entry);

		data[hash(entry.key)].push_back(entry);

	}

	template <class RecordType>
	void table<RecordType>::remove(int key)
	// Library facilities used: assert.h, link2.h
	{
		assert(key >= 0);

		typename std::vector<RecordType>::iterator it;

		for (it = data[hash(key)].begin() ; it != data[hash(key)].end(); ++it)
		{
			if ( it->key == key ) {
				data[hash(key)].erase (it );
				total_records--;
				return;
			}
		}
	}

	template <class RecordType>
	bool table<RecordType>::is_present(int key) const
	// Library facilities used: assert.h, stdlib.h
	{
		assert(key >= 0);

		typename std::vector<RecordType>::const_iterator it;
		for (it = data[hash(key)].begin() ; it != data[hash(key)].end(); ++it)
		{
			if ( it->key == key ) {
				//total_records--;
				return true;
			}
		}
		return false;
	}

	template <class RecordType>
	bool table<RecordType>::find(int key, RecordType& result) const
	// Library facilities used: stdlib.h
	{
		typename std::vector<RecordType>::const_iterator it;

		for (it = data[hash(key)].begin() ; it != data[hash(key)].end(); ++it)
		{
			if ( it->key == key ) {
				result = *it;
				return true;
			}
		}
		return false;
	}

	template <class RecordType>
	RecordType* table<RecordType>::find(int key)
	// Library facilities used: stdlib.h
	{
		typename std::vector<RecordType>::iterator it;
		RecordType* r;

		for (it = data[hash(key)].begin() ; it != data[hash(key)].end(); ++it)
		{
			if ( it->key == key ) {
				*r = *it;
				return r;
			}
		}
		return NULL;
	}

	template <class RecordType>
	std::size_t table<RecordType>::hash(int key) const
	// Library facilities used: stdlib.h
	{
		return (key % TABLE_SIZE);
	}

	template <class RecordType>
	RecordType table<RecordType>::operator [](size_t i) const
	{
		RecordType r;
		find(i,r);
		return r;
	}

	template <class RecordType>
	RecordType& table<RecordType>::operator [](size_t i)
	{
		RecordType* r;
		r = find(i);
		return (RecordType&)r;
	}
}

#endif
