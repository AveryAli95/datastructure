/*
 * lzwhelp.h
 *
 *  Created on: Nov 26, 2014
 *  Author: Miller
 */

#ifndef LZWHELP_H_
#define LZWHELP_H_

#include <vector>
#include <fstream>
#include <cassert>

namespace proj5 {

	//global constants
	const int MAXSIZE = 4099, //A prime # greater then 4096
	codes = 4096, 	// 2^12, max number of possible codes
	ByteSize = 8,	// size of char
	excess = 4,    // 12 - ByteSize
	alpha = 256,   // 2^ByteSize
	mask1 = 255,   // alpha - 1
	mask2 = 15;    // 2^excess - 1

	//void v8ToFile( std::ostream& out,std::vector<long> v)
	//	Precondition: out is a valid output file stream, v is a vector of numbers that can be represented in 8 bits
	//  Postcondition: character written for each vector element
	void v8ToFile(std::ostream& out,std::vector<long> v ) {
		for (std::vector<long>::iterator it = v.begin() ; it != v.end() ; ++it )
		{
			assert(*it < alpha); //Each # in vector must be in character range
			out << (unsigned char)(*it);
		}
	}

	//void v12ToFile( std::ostream& out,std::vector<long> v)
	//	Precondition: out is a valid output file stream, v is a vector of numbers that can be represented in 12 bits
	//  Postcondition: file written 12 bits at a time for each vector element
	void v12ToFile(std::ostream& out,std::vector<long> v )
	{
		long extra = -1;
		long c;
		for (std::vector<long>::iterator it = v.begin() ; it != v.end() ; ++it )
		{
			if (extra != -1 ) {
				out << (char)((extra << excess) | (*it >> ByteSize));
				out << (char)(*it&mask1);
				extra = -1;
			} else {
				c = (*it >> excess);
				out << (char)(c);
				extra = (*it & mask2);
			}
		}
	}

	//void v12FromFile(std::istream& in,std::vector<long>& v )
	//	Precondition: in is a valid input file stream, v is an empty vector
	//  Postcondition: v contains all 8bit characters read in from the in stream
	void v8FromFile( std::istream& in,std::vector<long>& v )
	{
		char c;
		while ( in.get(c) )
		{
			v.push_back((unsigned char)c);
		}
	}

	//void v12FromFile(std::istream& in,std::vector<long>& v )
	//	Precondition: in is a valid input file stream, v is an empty vector
	//  Postcondition: v contains all 12 bit values read in from the in stream
	void v12FromFile(std::istream& in,std::vector<long>& v )
	{
		char c, d;
		unsigned char ic,id;
		long code;
		long leftover=-1;

		while ( in.get(c) ) { // input 8 bits

			//get does not work with unsigned char, so need to cast after read in
			//need to use get instead of (c << in) since some spots may be spaces or other items skipped by <<
			ic = (unsigned char)c;

			// see if any left over bits from before
			// if yes, concatenate with left over 4 bits
			if (leftover == -1)
			{
				// no left over bits, need four more bits
				// to complete code
				if ( in.get(d) ) {// another 8 bits
					id = (unsigned char)d;
					code = (ic << excess) | (id >> excess);
				} else {
					code = ic;
				}
				v.push_back(code);
				leftover = id & mask2;
			}
			else
			{
				code = (leftover << ByteSize) | ic;
				v.push_back(code);
				leftover = -1;
			}
		}
	}
}



#endif /* LZWHELP_H_ */
