/*
Program Name: Maze
Programmer Name: Avery Ali
Date Submitted: 10/28/14
Purpose: Find best path through a maze through the use of stacks
Date Updated:na
Purpose for Update: na
Global Variable List: na
Variable List: 
x,x2 int hold row data from file
y,y2 int hold column data from file
*/
#include "outputhelp.h"
#include "position.h"
#include <fstream>
#include <iostream>
#include <stack>
using namespace std;
using namespace p3_maze;

int main(int argc, char * argv[])
{
	
	for(int b = 0; b < argc; b++)
	{
		cout << argv[b] << " ";
	}
	cout << endl;
	
	for(int a = 1; a < argc; a++)
	{
	
		ifstream mazeFile ( argv[a] );
		ifstream mazeFile2 ( argv[a] );
		int x,y,x2,y2;        //store columnes(y) and rows(x)

		mazeFile >> x >> y;  //read in column and rows from file
		mazeFile2 >> x2 >> y2;
	
		int maze[20][20]; //declare a 2d array for traversal
		int maze2[20][20];
		/*int ** maze = new int*[x2];
		for(int i = 0; i < x2; i++)
			maze[i] = new int[y2];

		int ** maze2 = new int*[x2];
		for(int i = 0; i < x2; i++)
			maze2[i] = new int[y2];
		*/
		for(int a = 0; a < 20; a++) //fills the entire mazearray with ones to create border
		{
			for(int b = 0; b < y2; b++)
			{
				maze[a][b] = 1;
			
			}
		}
		for(int a = 0; a < 20; a++) //fills the entire mazearray with ones to create border
		{
			for(int b = 0; b < y2; b++)
			{
				maze2[a][b] = 1;
			
			}
		}
	
		for(int i = 1; i <= x2; ++i)//fills maze by overwriting 1's
		{
			for(int k = 1; k <= y2; ++k)
			{
				mazeFile >> maze[i][k];
			}
		}
	
		for(int i = 1; i <= x2; ++i)//fills maze2 by overwriting 1's
		{
			for(int k = 1; k <= y2; ++k)
			{
				mazeFile2 >> maze2[i][k];
			}
		}
	
		stack<position> posStack; //first stack (move down then right)
		stack<position> posStack2; //second stack (move right then down)
	
		position travMaze = position();
		travMaze.setpos(1,1);
	
		position travMaze2 = position();
		travMaze2.setpos(1,1);

		posStack.push(travMaze);
		posStack2.push(travMaze2);
		travMaze.stepup();
		travMaze2.stepup();
//////////////////////////////////////First Stack///////////////////////////////////////		
		do
		{
			int temprow = travMaze.row();
		    int	tempcol = travMaze.col();
			
			if(maze[travMaze.row()][travMaze.col()+1] == 0) //move right 
			{
				travMaze.setpos(travMaze.row(),travMaze.col()+1); //moves right one
				maze[temprow][tempcol] = 1; //sets previous position to 0;
				posStack.push(travMaze); //push onto stack 
				travMaze.stepup();
			}
			
			else if(maze[travMaze.row()+1][travMaze.col()] == 0) //move down
			{
				travMaze.setpos(travMaze.row()+1,travMaze.col()); //moves down one
				maze[temprow][tempcol] = 1; //sets previous position to 0;
				posStack.push(travMaze); //push new position onto stack
				travMaze.stepup();
			}
	
			else if(maze[travMaze.row()][travMaze.col()-1] == 0) //move left 
			{
				travMaze.setpos(travMaze.row(),travMaze.col()-1); //moves up one
				maze[temprow][tempcol] = 1; //sets previous position to 0;
				posStack.push(travMaze); //push onto stack 
				travMaze.stepup();
			}
	
			else if(maze[travMaze.row()-1][travMaze.col()] == 0) //move up 
			{
				travMaze.setpos(travMaze.row()-1,travMaze.col()); //moves up one
				maze[temprow][tempcol] = 1; //sets previous position to 0;
				posStack.push(travMaze); //push onto stack 
				travMaze.stepup();
			}
	
			else if(maze[1][1] != maze[travMaze.row()][travMaze.col()])
			{
				maze[travMaze.row()][travMaze.col()] = 1; //sets current position to 1 so we dont come back to this dead end
				posStack.pop(); //pop to get last position
				position temp = posStack.top(); //get and store previous position 
				maze[temp.row()][temp.col()]=0;
				travMaze.setpos(temp.row(),temp.col()); //set previous position to current position
			}
			
			else
			{
				posStack.pop();
				break;
			}
			
		}while(travMaze.row() != x2 || travMaze.col() != y2);//end while
///////////////////////////////////////////SECOND STACK///////////////////////////////////////////////////////////////
		do
		{
			int temprow2 = travMaze2.row();
		    int	tempcol2 = travMaze2.col();
			
			if(maze2[travMaze2.row()+1][travMaze2.col()] == 0) //move down
			{
				travMaze2.setpos(travMaze2.row()+1,travMaze2.col()); //moves down one
				maze2[temprow2][tempcol2] = 1; //sets previous position to 0;
				posStack2.push(travMaze2); //push new position onto stack
				travMaze2.stepup();
			}
			
			else if(maze2[travMaze2.row()][travMaze2.col()+1] == 0) //move right 
			{
				travMaze2.setpos(travMaze2.row(),travMaze2.col()+1); //moves right one
				maze2[temprow2][tempcol2] = 1; //sets previous position to 0;
				posStack2.push(travMaze2); //push onto stack 
				travMaze2.stepup();
			}
			else if(maze2[travMaze2.row()-1][travMaze2.col()] == 0) //move up 
			{
				travMaze2.setpos(travMaze2.row()-1,travMaze2.col()); //moves up one
				maze2[temprow2][tempcol2] = 1; //sets previous position to 0;
				posStack2.push(travMaze2); //push onto stack 
				travMaze2.stepup();
			}

			else if(maze2[travMaze2.row()][travMaze2.col()-1] == 0) //move left 
			{
				travMaze2.setpos(travMaze2.row(),travMaze2.col()-1); //moves up one
				maze2[temprow2][tempcol2] = 1; //sets previous position to 0;
				posStack2.push(travMaze2); //push onto stack 
				travMaze2.stepup();
			}
	
			else if(maze2[1][1] != maze2[travMaze2.row()][travMaze2.col()])
			{
				maze2[travMaze2.row()][travMaze2.col()] = 1; //sets current position to 1 so we done come back to this dead end
				posStack2.pop(); //pop to get last position
				position temp2 = posStack2.top(); //get and store previous position 
				maze2[temp2.row()][temp2.col()] = 0;
				travMaze2.setpos(temp2.row(),temp2.col()); //set previous position to current position
			}

			else
			{
				posStack2.pop();
				break;
			}
		}while(travMaze2.row() != x2 || travMaze2.col() != y2);//end while
	
///////////////////////////////OUTPUT///////////////////////////////////////////////////		
		if( travMaze2.steps() >= travMaze.steps())
		{
			outProcessing(argv[a]);
			OutputPath(posStack);
		}

		else if (travMaze2.steps() <= travMaze.steps())
		{
			outProcessing(argv[a]);
			OutputPath(posStack2);
		}
	
		mazeFile.close();
		mazeFile2.close();
		/*for(int i = 0; i < x2; ++i)
		{
			delete maze[i];
			delete maze2[i];
		}
		delete maze;
		delete maze2;  */
	}//end first for
	return 0;
}//end main
