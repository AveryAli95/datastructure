/*
 * outputhelp.h
 *
 * The following functions are provided to make it a lot easier to match your output to the expected results.
 * It is recommended that your code contain NO direct cout calls.
 *
 * 	void OutputPath(ds_stack::stack<position> *path)
 *	Precondition: path is the top of the stack
 * 	Postcondition: The stack with me output to stdout.  STACK WILL BE FULLY POPPED!!
 *
 * 	void OutputPath(ds_stack::stack<position> path)
 *	Precondition: path is the top of the stack
 * 	Postcondition: The stack with me output to stdout.  STACK WILL BE FULLY POPPED!!
 * 	NOTE: This is a convenience function, you should be sending &path to OutputPath to avoid a COPY
 *
 *	void outUsage(char *thisCmd)
 *	Precondition: thisCmd is argv[0]
 *	Postcondition: The correct usage of the program will be output
 *
 *	outFileError(char* filename)
 *	Precondition: filename is the file name that was unable to be opened
 *	Postcondition: Error message is sent to stderr
 *
 *	void outProcessing(char* filename)
 *	Precondition: filename is the file name of a maze
 *	Postcondition: Status message sent to stdout
 */

#ifndef OUTPUTHELP_H_
#define OUTPUTHELP_H_

#include <iostream>
#include <stack>
#include "position.h"

namespace p3_maze {

	std::ostream& operator <<(std::ostream& outs, const position& source) {
		outs << "(" << source.row() << "," << source.col() << ")";
		return outs;
	}

	void OutputPath(std::stack<position> *path)
	{
		if ( !path->empty() ) {
			std::cout << "Optimal Path Found" << std::endl;
			position ptop;
			ptop = path->top();
			path->pop();
			std::cout << "Steps:" << ptop.steps() << std::endl;
			std::cout << "Path:" << ptop;

			while (!path->empty()) {
				ptop = path->top();
				path->pop();
				std::cout << "<-" << ptop;
			}
			std::cout << std::endl << std::endl;
		} else {
			std::cout << "No path found!" << std::endl << std::endl;
		}
	}

	void OutputPath(std::stack<position> path)
	{
		//This is not optimal since the programmer can use to overloaded function themselves by sending Address of the stacks path
		OutputPath(&path);
	}

	void outUsage(char *thisCmd) {
		std::cerr << "Usage: " << thisCmd << " mazefile [mazefile2] ... [mazefilen]" << std::endl;
	}

	void outFileError(char* filename) {
		std::cerr << filename << ": File unable to be opened for read." << std::endl;
	}

	void outProcessing(char* filename) {
		std::cout << filename << ": Processing Maze" << std::endl;
	}


}



#endif /* OUTPUTHELP_H_ */