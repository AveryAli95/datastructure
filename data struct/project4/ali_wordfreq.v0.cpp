/*
Program Name: Word Frequency
Programmer Name: Avery Ali
Date Submitted: 11/25/14
Purpose: Practice using binary search trees by searching through text files
Date Updated:na
Purpose for Update: na
Global Variable List: na
Variable List: frequenc<string> root , binary_tree_node<frequency<string>> tree, string word, char letter
*/
#include"bintree.h"
#include"frequency.h"
#include<string>
#include<iostream>
#include<cctype>
using namespace std;
using namespace proj4;

void insertWord( string, binary_tree_node<frequency<string>>&);

int main()
{
	string in;
	string storedtext;
////////////////////////test insertWord function///////
	/*binary_tree_node<frequency<string>> node1;
	string word = "banana";
	string word2 = "double";
	string word3 = "zebra";
	string word4 = "chocolate";
	node1.data().data_field = word;
	node1.data().freq_field =1;
	insertWord(word2,node1);
	insertWord(word,node1);
	insertWord(word3,node1);
	insertWord(word4,node1);
	insertWord(word4,node1);*/
//////////////////////////////////////////////////////
	

	binary_tree_node<frequency<string>> tree;
	frequency<string> * root = &tree.data(); //used for comparison 
	
	while(getline(cin,in))
	{
		string word;
		storedtext += in +"\n";	
		char lastletter = in[in.size()-1];
		//in contains the entire line in a string, parse string for words.
		for(int i = 0; i < in.size(); i++)
		{
			char letter = in[i]; //get the character
			if(i == in.size()-1) //last character case since there is no space after the last word 
			{
				word += tolower(lastletter);
				insertWord(word,tree);
				word = "";
			}

			if(isspace(letter) )
			{
				//put word into tree and set word back to empty string and continue loop
				if(tree.data().data() == root->data() )
				{
					//set root node to first word 
					root->data_field = word;
					root->freq_field = 1;
					tree.set_data(*root);
					word = "";
				}
				else 
				{ 
					insertWord(word,tree);
					word = "";
					
				} 

				continue;
			}
			else if(ispunct(letter) )
			{
				//ignore punctutation and continue until space
				continue;
			}
			else if(isdigit(letter) )
			{
				//ignore numbers and continue until space
				continue;
			}
			else
			word += tolower(letter); //at the lowercase character to the string 
			
		}
		//storedtext += in +"\n";		
	}
	
}//end main

void insertWord( string word, binary_tree_node<frequency<string>>& node)
{
	binary_tree_node<frequency<string>> * newnode = new binary_tree_node<frequency<string>>;
	newnode->data().data_field = word;
	newnode->data().freq_field = 1;

	int compare = node.data().data().compare(word); //compares node string to word 
	if(compare == 0) //base case
	{
		node.data().freq_field++; //if the node string is equal to word then increment freq
		newnode = NULL;
		delete newnode;
	}
	else if( compare > 0) //insert left
	{
		if(node.left() == NULL)
		{
			node.set_left( newnode );
			newnode = NULL;
			delete newnode;
		}
		else
		{
			newnode = NULL;
			delete newnode;
			insertWord(word,*node.left());
		}
	}
	else if( compare < 0) //insert right
	{
		if(node.right() == NULL)
		{
			node.set_right( newnode );
			newnode = NULL;
			delete newnode;
		}
		else
		{
			newnode = NULL;
			delete newnode;
			insertWord(word,*node.right());
		}
	}

}